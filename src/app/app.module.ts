import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { ClientsComponent } from './clients/clients.component';
import { CommunicationsComponent } from './communications/communications.component';
import { OrdersComponent } from './orders/orders.component';
import { TasksComponent } from './tasks/tasks.component';

import { ClientService } from './client.service';

const appRoutes: Routes = [
{
  path: 'clients',
  component: ClientsComponent
},
{
  path: 'communications',
  component: CommunicationsComponent
},
{
  path: 'orders',
  component: OrdersComponent
},
{
  path: 'tasks',
  component: TasksComponent
}
];

@NgModule({
  declarations: [
    AppComponent,
    ClientsComponent,
    CommunicationsComponent,
    OrdersComponent,
    TasksComponent
  ],
  imports: [
    NgbModule.forRoot(),
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    HttpModule,
  ],
  providers: [ClientService],
  bootstrap: [AppComponent]
})
export class AppModule { }
