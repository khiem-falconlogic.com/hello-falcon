import { Injectable } from '@angular/core';

import { Http } from '@angular/http';
import 'rxjs/add/operator/map';


@Injectable()
export class ClientService {

  result: any;
  
  constructor(private _http: Http) { }

  getClients() {
    return this._http.get("/api/clients")
      .map(result => this.result = result.json());
  }
}
