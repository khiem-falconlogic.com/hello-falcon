import { Component, OnInit } from '@angular/core';

import { ClientService } from '../client.service';
import { Client } from '../client';


@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.css']
})
export class ClientsComponent implements OnInit {

  clients: Array<Client>;
  

  constructor(private _clientService: ClientService) { }

  ngOnInit() {
    this._clientService.getClients()
      .subscribe(res => this.clients = res);
   
  }

}
