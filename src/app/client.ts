export class Client {
    _id: string;
    name: string;
    email: string;
    address: string;
    description: string;
}
