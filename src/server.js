const express = reqiure('express');
const http = reqiure('http');

const app = express();

// Serve static files
app.use(express.static(path.join(__dirname, 'dist')));

// Set port
const port = process.env.PORT || '3000';
app.set('port', port);

// Create the HTTP Server
const server = http.createServer(app);
server.listen(port, () => console.log(`Server running on localhost:${port}`));
