
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const postSchema = new Schema({
    name: String,
    email: String,
    address: String,
    description: String
});

module.exports = mongoose.model('client', postSchema);
