const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

const client = require('../models/client');

// Connect to MongoDB colection
const db = "mongodb://khiem:falcon@ds139964.mlab.com:39964/falconclients";

mongoose.Promise = global.Promise;
mongoose.connect(db, function(err) {
    if(err) {
        console.log('Connection error');
    }
});

router.get('/clients', function(req, res){
    console.log('Requesting clients');
    client.find({})
        .exec(function(err, clients){
            if(err){
                console.log("Error getting the clients");
            } else {
                res.json(clients);
                console.log(clients);
            }
        });
});

module.exports = router;